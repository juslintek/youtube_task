# INSTALL INSTRUCTIONS
Uses regular laravel project setup:

1. clone repository
2. composer install
3. run php artisan serve
4. or vagrant up && npm install; npm run watch;

# DESCRIPTION
### Default screen
1. Uses precreated youtube api communication package, which uses curl to communicate via REST directly.
2. Via the package fetches most popular videos in Ireland
3. Allows to search youtube videos.
4. Ajax communication is done via axios npm library 
5. Visual real time representation is done via vue2.js and bootstrap-4

### Alternative screen

Same as above, just uses google api sdk.

P.S. Google API sdk, has a lot more features which can be adapted.

You can change api key at `app\config\youtube.php`

## Few words from author
You can use this as boilerplate for your personal youtube api based project.