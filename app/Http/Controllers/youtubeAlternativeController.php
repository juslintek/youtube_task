<?php

namespace App\Http\Controllers;

use Google_Client;
use Google_Exception;
use Google_Service_Exception;
use Google_Service_YouTube;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Session;

class YoutubeAlternativeController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request) {
        $api_key = config('youtube.KEY');
        $client = new Google_Client();
        $client->setDeveloperKey(config('youtube.KEY'));

        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);

        try {

            $videos = [];

            // Call the video.list method to retrieve most popular results
            /** @var \Google_Service_YouTube_Video $google_service_youtube_video */
            foreach ($youtube->videos->listVideos('id, snippet, player', [
                'chart' => 'mostPopular',
                'regionCode' => 'ie',
                'maxResults' => 10,
            ])->getItems() as $google_service_youtube_video){
                array_push($videos, [
                    'id'=>$google_service_youtube_video->getId(),
                    'player' => $google_service_youtube_video->getPlayer(),
                    'snippet' => $google_service_youtube_video->getSnippet()
                ]);
            }

            return response()->json($videos);

        } catch (Google_Service_Exception $e) {
            return response()->json(\GuzzleHttp\json_decode($e->getMessage())->error->errors, 400);
        } catch (Google_Exception $e) {
            return response()->json(\GuzzleHttp\json_decode($e->getMessage())->error->errors, 400);
        }
    }

    public function search(Request $request) {
        $api_key = config('youtube.KEY');
        $client = new Google_Client();
        $client->setDeveloperKey(config('youtube.KEY'));

        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);

        try {

            $videos = [];

            // Call the video.list method to retrieve most popular results
            /** @var \Google_Service_YouTube_SearchResult $google_search_result */
            foreach ($youtube->search->listSearch('id, snippet', [
                'maxResults' => 10,
                'videoEmbeddable' => 'true',
                'videoSyndicated' => 'true',
                'type' => 'video',
                'q' => $request->search
            ])->getItems() as $google_search_result){$google_search_result->getId();
                $id = $google_search_result->getId();
                $snippet = $google_search_result->getSnippet();
                array_push($videos, [
                    'id' => $id,
                    'snippet' => $snippet,
                    'player' => [
                        'embedHtml' => '<iframe src="//www.youtube.com/embed/'. $id->videoId .'"></iframe>'
                     ]
                ]);
            }

            return response()->json($videos);

        } catch (Google_Service_Exception $e) {
            return response()->json(\GuzzleHttp\json_decode($e->getMessage())->error->errors, 400);
        } catch (Google_Exception $e) {
            return response()->json(\GuzzleHttp\json_decode($e->getMessage())->error->errors, 400);
        }
    }
}
