<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alaouy\Youtube\Facades\Youtube;

class YoutubeController extends Controller
{
    public function index() {
        $api_key = config('youtube.KEY');
        return response()->json(Youtube::getPopularVideos('ie'));
    }

    public function search(Request $request) {
        $videos = [];
        foreach (Youtube::searchVideos($request->search) as $video) {
            array_push($videos, Youtube::getVideoInfo($video->id->videoId));
        }
        return response()->json($videos);
    }
}
