<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return view('welcome');
});
Route::post('/youtube-index', 'YoutubeController@index');
Route::post('/youtube-search', 'YoutubeController@search');

Route::group(['prefix' => 'alternative'], function () {
    Route::post('youtube-index', 'YoutubeAlternativeController@index')->name('alternative-youtube');
    Route::get('youtube-index', 'YoutubeAlternativeController@index')->name('alternative-youtube');
    Route::post('youtube-search', 'YoutubeAlternativeController@search')->name('alternative-youtube-search');
});
